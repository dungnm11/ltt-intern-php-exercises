<?php

require(__DIR__ . '/../src/tree.php');

use PHPUnit\Framework\TestCase;

class TreeTest extends TestCase
{
    public function testTree(): void
    {
        $dataTest = json_decode(file_get_contents(__DIR__ . '/data_test/nodes.json'));
        $result = file_get_contents(__DIR__ . '/data_test/result.html');
        $tree = new Tree;
        $tree->init($dataTest);
        $this->assertEquals($tree->toString(), $result);
    }
}

<?php

interface TreeInterface
{
    public function init($array);
    public function getNode($id);
    public function getNodes($id);
    public function getParent($id);
    public function toString(); // Print nested <ul, li>

    // you can add more functions
}

class Node
{
    protected $id;
    protected $parentId;
    protected $name;

    // you can add more attributes or functions
}

class Tree implements TreeInterface
{
    // insert code here
}

